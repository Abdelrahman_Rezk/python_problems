'''
	Abdelrahman Rezk 25 - 6 - 2019
	Web Developer
'''
num = 0
dirx = [0,0,1,-1,-1,-1,1,1];
diry = [1,-1,0,0,1,-1,1,-1];

def valid_step(r , c):
    return (r>=0 and c>=0 and r<n and c<n)

def dfs(r, c):
    if valid[r][c]:
        return
    valid[r][c] = 1
    for i in range(8):
        dx = dirx[i] + r
        dy = diry[i] + c
        if valid_step(dx, dy )and ch[dx][dy] == '1':
            dfs(dx, dy)

while True:
    try:
        n = int(input())
    except EOFError as e:
        break;
    valid = [[0 for col in range(100)]for row in range(100)]
    ch = []
    cnt = 0
    num +=1
    for i in range(n):
        ch.append(input())
    for i in range(n):
        for g in range(n):
            if not valid[i][g] and ch[i][g] =='1':
                dfs(i, g)
                cnt +=1
    print("Image number {} contains {} war eagles.".format(num, cnt))
