#include            <bits/stdc++.h>
#define             ll long long
#define             lep_0(i,n) for(int i=0;i<n;++i)
#define             lep_1(i,n) for(int i=1;i<n;++i)
#define             lep_1_to_n(i,n) for(int i=1;i<=n;++i)
#define             sz(v) (int)(v.size())

using namespace std;

void print_num(int n){
    if(n){
        print_num(n/10);
        printf("%d", n % 10);
    }
}
void print_num_rever(int n){
    if(n){
        printf("%d", n % 10);
        print_num_rever(n/10);
    }
}
void number_bits(int n){
    if(n){
        number_bits(n>>1);
        printf("%d", n&1);
    }
}


int main()
{ 

 
    print_num(123456);
    puts("");
    print_num_rever(123456);
    puts("");
    number_bits(26);
    puts("");
 	
    
    return 0;
} 