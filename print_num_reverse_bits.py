def print_num(n):
	if n:
		print_num(n//10)
		print(n%10, end='')

def print_num_rever(n):
	if n:
		print(n%10, end='')
		print_num_rever(n//10)

def number_bits(n):
	if n:
		number_bits(n//2)
		print(n&1, end='')


print_num(123456);
print()
print_num_rever(123456);
print()
number_bits(26);
print()
